#####################################################################
# What is a KMS key?
#####################################################################

As part of the enterprise deployment, many of Microsoft's corporate customers have set up Key Management Services (KMS) to facilitate the rapid activation of WINDOWS10 in their environment. This is a simple process in which the KMS host is set up first, and then the KMS client discovers the KMS host and attempts to connect activation.

#####################################################################
# How to use KMS Key?
#####################################################################

Computers that are running volume licensed editions of Windows 10, Windows 8.1/8, Windows 7, Windows Server 2012 (R2), Windows Server 2008 (R2), including Windows Vista are, by default, KMS clients with no additional configuration needed.

To use the product keys listed here (which are GVLKs), you must first have a KMS host running in your deployment. If you haven't already configured a KMS host, see Deploy KMS Activation for steps to set one up.

If you are converting a computer from a KMS host, MAK, or retail edition of Windows 10 to a KMS client, install the applicable setup key (GVLK) from the following tables. To install a client setup key, open an administrative command prompt on the client, type slmgr /ipk <setup key> and then press Enter.

[Download Windows 10 Disc Image (ISO File)](https://www.microsoft.com/software-download/windows10)

[Free latest Windows 10 & Office 2016 KMS Keys](https://bitbucket.org/key1024/windows-10-keys/src/091985148b939e77d02c5a830c6128d6e399a763/Windows-10-KMS-Keys.txt?at=master&fileviewer=file-view-default)

[Get a new Windows 10 product key](https://www.key1024.com/software/microsoft-windows/windows-10-product-key/) here, like [Windows 10 Pro Product Key 32/64 Bit (Retail Version)](https://www.key1024.com/buy-windows-10-pro-product-key-64-bit-cheap-price.html)

#####################################################################
# How to use cmd.exe to Install/Uninstall KMS keys                                            #
#####################################################################

1.) Uninstall the current product by entering the uninstall product key extension:
slmgr.vbs /upk

2.) Install the key that you obtained below for KMS key for Windows 10 Professional N version 1803
slmgr.vbs /ipk <xxxxx-xxxxx-xxxxx-xxxxx-xxxxx>

3.) Verify that the key took by executing a Detailed License View:
slmgr.vbs /dlv

4.) Last step is to activate it against the server
cscript slmgr.vbs /skms keyserver.yourdomain.com
 
#####################################################################
# Free latest Windows 10 KMS Keys:
# https://docs.microsoft.com/en-us/windows-server/get-started/kmsclientkeys #
#####################################################################


|Windows 10 version 1803|KMS Product Keys|
|Windows 10 Education|NW6C2-QMPVW-D7KKK-3GKT6-VCFB2|
|Windows 10 Education N|2WH4N-8QGBV-H22JP-CT43Q-MDWWJ|
|Windows 10 Enterprise|NPPR9-FWDCX-D2C8J-H872K-2YT43|
|Windows 10 Enterprise G|YYVX9-NTFWV-6MDM3-9PT4T-4M68B|
|Windows 10 Enterprise G N|44RPN-FTY23-9VTTB-MP9BX-T84FV|
|Windows 10 Enterprise N|DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4|
|Windows 10 Professional|W269N-WFGWX-YVC9B-4J6C9-T83GX|
|Windows 10 Professional Education|6TP4R-GNPTD-KYYHQ-7B7DP-J447Y|
|Windows 10 Professional Education N|YVWGF-BXNMC-HTQYQ-CPQ99-66QFC|
|Windows 10 Professional N|MH37W-N47XK-V7XM9-C7227-GCQG9|
|Windows 10 Professional Workstation|NRG8B-VKK3Q-CXVCJ-9G2XF-6Q84J|
|Windows 10 Professional Workstation N|9FNHH-K3HBT-3W4TD-6383H-6XYWF|


|Windows 10|KMS Serial Keys|
|Windows 10 Professional|W269N-WFGWX-YVC9B-4J6C9-T83GX|
|Windows 10 Professional N|MH37W-N47XK-V7XM9-C7227-GCQG9|
|Windows 10 Enterprise|NPPR9-FWDCX-D2C8J-H872K-2YT43|
|Windows 10 Enterprise N|DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4|
|Windows 10 Education|NW6C2-QMPVW-D7KKK-3GKT6-VCFB2|
|Windows 10 Education N|2WH4N-8QGBV-H22JP-CT43Q-MDWWJ|
|Windows 10 Enterprise 2015 LTSB|WNMTR-4C88C-JK8YV-HQ7T2-76DF9|
|Windows 10 Enterprise 2015 LTSB N|2F77B-TNFGY-69QQF-B8YKP-D69TJ|
|Windows 10 Enterprise 2016 LTSB|DCPHK-NFMTC-H88MJ-PFHPY-QJ4BJ|
|Windows 10 Enterprise 2016 LTSB N|QFFDN-GRT3P-VKWWX-X7T3R-8B639|


|Windows 10, version 1709|KMS Activation Keys|
|Windows 10 Professional Workstation|NRG8B-VKK3Q-CXVCJ-9G2XF-6Q84J|
|Windows 10 Professional Workstation N|9FNHH-K3HBT-3W4TD-6383H-6XYWF|